package org.rose.main;

import java.io.IOException;
import java.util.Scanner;

import org.rose.domain.HealthBean;

public class CalculateInsurancePlan 
{
	public static void main(String[] args) throws Exception
	{
		CalculateInsurancePlan cal=new CalculateInsurancePlan();
		Scanner sc=new Scanner(System.in);
		HealthBean hb=new HealthBean();
		
		System.out.println("Name: ");
		hb.setName(sc.nextLine());
		
		System.out.println("Gender: ");
		hb.setGender(sc.nextLine());
		
		System.out.println("Age in years: ");
		hb.setAge(sc.nextInt());
		
		System.out.println("Current Health:");
		
		System.out.println("Hypertension (Yes/No): ");
		hb.setHipertension(sc.next());
		
		System.out.println("Blood pressure (Yes/No): ");
		hb.setBloodPressure(sc.next());
		
		System.out.println("Blood Suger (Yes/No) :");
		hb.setBloodSugar(sc.next());
		
		System.out.println("Overwieght (Yes/No): ");
		hb.setOverWeight(sc.next());
		
		System.out.println("Habits:");
		System.out.println("Smoking (Yes/No): ");
		hb.setSmoking(sc.next());
		System.out.println("Alcohol (Yes/No): ");
		hb.setAlcohol(sc.next());
		System.out.println("Daily exercise (Yes/No) :");
		hb.setDailyExcercise(sc.next());
		System.out.println("Drugs (Yes/No): ");
		hb.setDrugs(sc.next());
		
		
		System.out.println("Health Insurance Premium for Mr."+hb.getName()+": Rs."+cal.calculateHealthPlan(hb));
	}

	public double calculateHealthPlan(HealthBean hb) 
	{
		double basePremium=5000,reducePremium = 5000;
		if(18<hb.getAge()&&hb.getAge()<40)
		{
			basePremium=basePremium+(basePremium*0.1);
			
			if((25<hb.getAge()))
			{
				basePremium=basePremium+(basePremium*0.1);
			}
			if((30<hb.getAge()))
			{
				basePremium=basePremium+(basePremium*0.1);
			}
			if(35<hb.getAge())
			{
				basePremium=basePremium+(basePremium*0.1);
			}
		}
		if(40<=hb.getAge()&&45<hb.getAge())
		{
			basePremium=basePremium+(basePremium*0.2);
		}
		if(45<=hb.getAge()&&50<hb.getAge())
		{
			basePremium=basePremium+(basePremium*0.2);
		}
		if(50<=hb.getAge()&&55<hb.getAge())
		{
			basePremium=basePremium+(basePremium*0.2);
		}
		if(55<=hb.getAge()&&60<hb.getAge())
		{
			basePremium=basePremium+(basePremium*0.2);
		}
		
		
		
		if(hb.getGender().equalsIgnoreCase("male"))
		{
			basePremium=basePremium+(basePremium*0.02);
		}
		
		if(hb.getHipertension().equalsIgnoreCase("yes"))
		{
			basePremium=basePremium+(basePremium*0.01);
		}
		if(hb.getBloodPressure().equalsIgnoreCase("yes"))
		{
			basePremium=basePremium+(basePremium*0.01);
		}
		if(hb.getBloodSugar().equalsIgnoreCase("yes"))
		{
			basePremium=basePremium+(basePremium*0.01);
		}
		if(hb.getOverWeight().equalsIgnoreCase("yes"))
		{
			basePremium=basePremium+(basePremium*0.01);
		}
		
		//good Habits
		
		if(hb.getDailyExcercise().equalsIgnoreCase("yes"))
		{
			reducePremium=basePremium-(basePremium*0.03);
		}
		
		//bad habits
		if(hb.getSmoking().equalsIgnoreCase("yes"))
		{
			basePremium=reducePremium+(basePremium*0.03);
			
		}
		if(hb.getAlcohol().equalsIgnoreCase("yes"))
		{
			basePremium=reducePremium+(basePremium*0.03);
		}
		if(hb.getDrugs().equalsIgnoreCase("yes"))
		{
			basePremium=basePremium+(basePremium*0.03);
		}
		
		return round(basePremium,0);
	}
	
	public static double round(double value, int places) 
	{
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}

}