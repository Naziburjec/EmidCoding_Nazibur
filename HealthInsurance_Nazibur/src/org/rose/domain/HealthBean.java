package org.rose.domain;

public class HealthBean {
	private String name;
	private String gender;
	private int age;
	
	private String hipertension;
	private String bloodPressure;
	private String bloodSugar;
	private String overWeight;
	
	private String smoking;
	private String alcohol;
	private String dailyExcercise;
	private String drugs;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getHipertension() {
		return hipertension;
	}
	public void setHipertension(String hipertension) {
		this.hipertension = hipertension;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public String getBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public String getOverWeight() {
		return overWeight;
	}
	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getDailyExcercise() {
		return dailyExcercise;
	}
	public void setDailyExcercise(String dailyExcercise) {
		this.dailyExcercise = dailyExcercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
	
	
	

}
