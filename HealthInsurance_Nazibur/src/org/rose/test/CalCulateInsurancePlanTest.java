package org.rose.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.rose.domain.HealthBean;
import org.rose.main.CalculateInsurancePlan;

public class CalCulateInsurancePlanTest 
{
	HealthBean hb=new HealthBean();	
	
	@Before
	public void setUp()
	{
				
		hb.setName("Norman Gomes");
		hb.setGender("Male");
		hb.setAge(34);
		
		hb.setHipertension("No");
		hb.setBloodPressure("No");
		hb.setBloodSugar("No");
		hb.setOverWeight("Yes");
		hb.setSmoking("No");
		hb.setAlcohol("Yes");
		hb.setDailyExcercise("yes");
		hb.setDrugs("No");
	}
	
	@Test
	public void CalculateInsurancePlanTest()
	{
		CalculateInsurancePlan cal=new CalculateInsurancePlan();
		Assert.assertEquals(6856, cal.calculateHealthPlan(hb),0.0);
	}

}
